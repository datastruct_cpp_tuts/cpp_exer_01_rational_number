// Your goal is to create a rational number class that would
// support each of the operations given in main.cpp.
//
// In this file you must declare only the interface of your class
// and implement the given functions separately from the class (at the bottom of
// this file inside the namespace).
// Notice that the RationalNumber is a class template, where the
// template parameter is an integer type for numerator and denominator.
//
// Note - Rename the namespace "yourname" to whatever you want, feel creative
// ( but not too much :) ).
//
// After you wrote RationalNumber class and the tests in the main function work
// - write at the bottom of the file the downsides of such rational numbers,
// what would you change/remove/add? Would you use such rational numbers instead
// of double/float numbers? There is no right/wrong answer, this question is
// more of a philosofical kind than technical.

#include <cmath>
#include <iostream>
#include <new>

namespace mynameis {

template <typename T> class RationalNumber {
private:
  T m_numerator;
  T m_denominator;

public:
  RationalNumber(T numerator, T denominator);
  RationalNumber(T number);
  RationalNumber();

  template <typename P>
  friend std::ostream &operator<<(std::ostream &os,
                                  const RationalNumber<P> &obj);

  RationalNumber &operator+=(const RationalNumber &other);
  RationalNumber &operator-=(const RationalNumber &other);
  RationalNumber &operator*=(const RationalNumber &other);
  RationalNumber &operator/=(const RationalNumber &other);

  // prefix
  RationalNumber &operator++();
  // postfix
  RationalNumber operator++(int value);

  RationalNumber operator+(const RationalNumber &other);
  RationalNumber operator-(const RationalNumber &other);
  RationalNumber operator*(const RationalNumber &other);
  RationalNumber operator/(const RationalNumber &other);

  bool operator<(const RationalNumber &other);
  bool operator>(const RationalNumber &other);
  bool operator<=(const RationalNumber &other);
  bool operator>=(const RationalNumber &other);
  bool operator==(const RationalNumber &other);
  bool operator!=(const RationalNumber &other);

  // template <typename P>
  // friend RationalNumber operator+(const RationalNumber<P> &rat, const int
  // num); template <typename P> friend RationalNumber operator*(const
  // RationalNumber<P> &rat, const int num);

  RationalNumber &operator+();
  RationalNumber &operator-();

  operator int() const;
  operator float() const;
  operator double() const;

  void prettify();

  static T gcd(T a, T b);
  static T lcm(T a, T b);
  static T commDiv(T a, T b);
};

namespace literals {

  inline RationalNumber<unsigned long long> operator ""_r(unsigned long long num) {
    return RationalNumber<unsigned long long>(num);
  }
}

// constructors
template <typename T>
RationalNumber<T>::RationalNumber(T numerator, T denominator)
    : m_numerator(numerator), m_denominator(denominator) {
  prettify();
};

template <typename T>
RationalNumber<T>::RationalNumber(T number)
    : m_numerator(number), m_denominator(1){};

template <typename T>
RationalNumber<T>::RationalNumber() : m_numerator(0), m_denominator(1) {}

// operators
// <<
template <typename T>
std::ostream &operator<<(std::ostream &os, const RationalNumber<T> &obj) {
  os << obj.m_numerator << '/' << obj.m_denominator;
  return os;
}

// ++
template <typename T>
RationalNumber<T> RationalNumber<T>::operator++(int value) {
  RationalNumber<T> copy = *this;
  operator++();
  return copy;
}

template <typename T> RationalNumber<T> &RationalNumber<T>::operator++() {
  RationalNumber<int> one(1, 1);
  *this += one;
  return *this;
}

// +=
template <typename T>
RationalNumber<T> &
RationalNumber<T>::operator+=(const RationalNumber<T> &other) {
  // T common_denominator = this->m_denominator * other.m_denominator;
  T common_denominator = lcm(this->m_denominator, other.m_denominator);
  T left_numerator =
      common_denominator / this->m_denominator * this->m_numerator;
  T right_numberator =
      common_denominator / other.m_denominator * other.m_numerator;
  this->m_numerator = left_numerator + right_numberator;
  this->m_denominator = common_denominator;
  prettify();
  return *this;
}

template <typename T>
RationalNumber<T> &
RationalNumber<T>::operator-=(const RationalNumber<T> &other) {
  T common_denominator = lcm(this->m_denominator, other.m_denominator);
  T left_numerator =
      common_denominator / this->m_denominator * this->m_numerator;
  T right_numberator =
      common_denominator / other.m_denominator * other.m_numerator;
  this->m_numerator = left_numerator - right_numberator;
  this->m_denominator = common_denominator;
  prettify();
  return *this;
}

template <typename T>
RationalNumber<T> &
RationalNumber<T>::operator*=(const RationalNumber<T> &other) {
  T topDiv = commDiv(this->m_numerator, other.m_denominator);
  T bottomDiv = commDiv(other.m_numerator, this->m_denominator);

  this->m_numerator = this->m_numerator / topDiv * (other.m_numerator / bottomDiv);
  this->m_denominator = this->m_denominator / bottomDiv * (other.m_denominator / topDiv);

  prettify();
  return *this;
}

template <typename T>
RationalNumber<T> &
RationalNumber<T>::operator/=(const RationalNumber<T> &other) {
  T final_numerator = this->m_numerator * other.m_denominator;
  T final_denominator = other.m_numerator * this->m_denominator;

  this->m_numerator = final_numerator;
  this->m_denominator = final_denominator;
  prettify();
  return *this;
}

// +
template <typename T>
RationalNumber<T> RationalNumber<T>::operator+(const RationalNumber<T> &other) {
  RationalNumber<T> result = *this;
  return result += other;
}

template <typename T>
RationalNumber<T> RationalNumber<T>::operator-(const RationalNumber<T> &other) {
  RationalNumber<T> result = *this;
  return result -= other;
}

template <typename T>
RationalNumber<T> RationalNumber<T>::operator*(const RationalNumber<T> &other) {
  RationalNumber<T> result = *this;
  return result *= other;
}

template <typename T>
RationalNumber<T> RationalNumber<T>::operator/(const RationalNumber<T> &other) {
  RationalNumber<T> result = *this;
  return result /= other;
}

// int
// template <typename T>
// RationalNumber<T> operator+(const RationalNumber<T> &rat, const int num) {
//   RationalNumber<T> result(num);
//   return result += rat;
// }
//
// template <typename T>
// RationalNumber<T> operator*(const RationalNumber<T> &rat, const int num) {
//   RationalNumber<T> result(num);
//   return result -= rat;
// }

template <typename T> RationalNumber<T> &RationalNumber<T>::operator+() {
  return *this;
}
template <typename T> RationalNumber<T> &RationalNumber<T>::operator-() {
  return *this *= RationalNumber<T>(-1);
}

// cast
template <typename T> RationalNumber<T>::operator int() const {
  return (int)this->m_numerator / (int)this->m_denominator;
}

template <typename T> RationalNumber<T>::operator float() const {
  return (float)this->m_numerator / (float)this->m_denominator;
}

template <typename T> RationalNumber<T>::operator double() const {
  return (double)this->m_numerator / (double)this->m_denominator;
}

// compare
template <typename T>
bool RationalNumber<T>::operator<(const RationalNumber &other) {
  T common_denominator = this->m_denominator * other.m_denominator;
  T left_numerator =
      common_denominator / this->m_denominator * this->m_numerator;
  T right_numberator =
      common_denominator / other.m_denominator * other.m_numerator;
  return left_numerator < right_numberator;
}
template <typename T>
bool RationalNumber<T>::operator>(const RationalNumber &other) {

  T common_denominator = this->m_denominator * other.m_denominator;
  T left_numerator =
      common_denominator / this->m_denominator * this->m_numerator;
  T right_numberator =
      common_denominator / other.m_denominator * other.m_numerator;
  return left_numerator > right_numberator;
}
template <typename T>
bool RationalNumber<T>::operator<=(const RationalNumber &other) {

  T common_denominator = this->m_denominator * other.m_denominator;
  T left_numerator =
      common_denominator / this->m_denominator * this->m_numerator;
  T right_numberator =
      common_denominator / other.m_denominator * other.m_numerator;
  return left_numerator <= right_numberator;
}
template <typename T>
bool RationalNumber<T>::operator>=(const RationalNumber &other) {

  T common_denominator = this->m_denominator * other.m_denominator;
  T left_numerator =
      common_denominator / this->m_denominator * this->m_numerator;
  T right_numberator =
      common_denominator / other.m_denominator * other.m_numerator;
  return left_numerator >= right_numberator;
}

template <typename T>
bool RationalNumber<T>::operator==(const RationalNumber &other) {

  T common_denominator = this->m_denominator * other.m_denominator;
  T left_numerator =
      common_denominator / this->m_denominator * this->m_numerator;
  T right_numberator =
      common_denominator / other.m_denominator * other.m_numerator;
  return left_numerator == right_numberator;
}
template <typename T>
bool RationalNumber<T>::operator!=(const RationalNumber &other) {
  T common_denominator = this->m_denominator * other.m_denominator;
  T left_numerator =
      common_denominator / this->m_denominator * this->m_numerator;
  T right_numberator =
      common_denominator / other.m_denominator * other.m_numerator;
  return left_numerator != right_numberator;
}

// utils

template <typename T> void RationalNumber<T>::prettify() {
  T commDiv =
      RationalNumber<T>::commDiv(this->m_numerator, this->m_denominator);
  this->m_numerator /= commDiv;
  this->m_denominator /= commDiv;

  if (this->m_denominator < 0) {
    this->m_numerator *= -1;
    this->m_denominator *= -1;
  }
}

// Function to return gcd of a and b
template <typename T> T RationalNumber<T>::gcd(T a, T b) {
  // Everything divides 0
  if (a == 0)
    return b;
  if (b == 0)
    return a;

  // base case
  if (a == b)
    return a;

  // a is greater
  if (a > b)
    return gcd(a - b, b);
  return gcd(a, b - a);
}

// function to calculate
// lcm of two numbers.
template <typename T> T RationalNumber<T>::lcm(T a, T b) {
  return a / gcd(a, b) * b;
}

template <typename T> T RationalNumber<T>::commDiv(T a, T b) {
  for (T i = std::min(a, b); i > 1; i--) {
    if ((a % i == 0) && (b % i == 0))
      return i;
  }
  return 1;
}

} // namespace mynameis
