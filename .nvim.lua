USER_STARTUP.restore_session_au()

vim.api.nvim_create_autocmd("User", {
	group = "User_startup_autgroup",
	pattern = "VeryLazy",
	callback = function()
		print("startup finished")
	end,
})

vim.cmd("set expandtab")

vim.api.nvim_create_autocmd("User", {
	group = "User_startup_autgroup",
	pattern = "VeryLazy",
	callback = function()
		vim.cmd("Lazy load overseer.nvim")
		local overseer = require("overseer")
		overseer.register_template({
			name = "Clean code",
			builder = function(params)
				return {
					cmd = { "rm" },
					args = { "*.h.gch" },
				}
			end,
		})
		overseer.register_template({
			name = "Build code",
			builder = function(params)
				return {
					cmd = { "g++" },
					args = { "main.cpp", "RationalNumber.h" },
				}
			end,
		})
		overseer.register_template({
			name = "Run code",
			builder = function(params)
				return {
					cmd = { "./a.out" },
				}
			end,
		})
		overseer.register_template({
			name = "Run code (All)",
			builder = function(params)
				return {
					cmd = { "./a.out" },
					components = {
						{
							"dependencies",
							task_names = {
								"Clean code",
								"Build code",
							},
						},
					},
				}
			end,
		})
	end,
})
